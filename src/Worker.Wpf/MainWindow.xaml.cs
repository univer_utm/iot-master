﻿using System;
using System.IO;
using System.Windows;
using Ozeki.Camera;
using Ozeki.Media;
using Worker.Wpf.Helpers;

namespace Worker.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IIPCamera _camera;
       
        private DlibDrawingImageProvider _displayImageProvider = new DlibDrawingImageProvider(new DrawingImageProvider());
        private MediaConnector _connector = new MediaConnector();
        private VideoViewerWF _videoViewerWf = new VideoViewerWF();

        public MainWindow()
        {
            InitializeComponent();
            _camera = new IPCamera("192.168.1.6:5000", "admin", "Master.1996");
            //_camera = IPCameraFactory.GetCamera("rtsp://admin:Master.1996@192.168.1.6:554/onvif1", "", "", CameraTransportType.UDP, true);
            var webCamera = new WebCamera();

            _camera.CameraStateChanged += CameraOnCameraStateChanged;
            var connected = _connector.Connect(webCamera.VideoChannel, _displayImageProvider.InputImageProvider);
            _videoViewerWf.SetImageProvider(_displayImageProvider);
            _camera.CameraErrorOccurred += CameraOnCameraErrorOccurred;
            _videoViewerWf.Start();
            //_camera.Start();
            webCamera.Start();

            _camera.VideoChannel.MediaDataSent += VideoChannelOnMediaDataSent;

            _displayImageProvider.ImageReady += (sender, args) =>
            {
                args.Item.Save(Path.Combine(AppContext.BaseDirectory, "test.png"));
            };
        }

        private void CameraOnCameraErrorOccurred(object sender, CameraErrorEventArgs e)
        {

        }

        private void VideoChannelOnMediaDataSent(object sender, VideoData e)
        {

        }

        private void CameraOnCameraStateChanged(object sender, CameraStateEventArgs e)
        {

        }
    }
}

﻿using System.Drawing;
using Ozeki.Media;
using DlibDotNet;
using DlibDotNet.Extensions;
using Ozeki.Common;
using Dlib = DlibDotNet.Dlib;

namespace Worker.Wpf.Helpers
{
    public class DlibDrawingImageProvider : DrawingImageProvider
    {
        public DrawingImageProvider InputImageProvider { get; }

        public DlibDrawingImageProvider(DrawingImageProvider inputImageProvider)
        {
            InputImageProvider = inputImageProvider;
            InputImageProvider.ImageReady += ImageProviderOnImageReady;
        }

        private void ProcessFrame(Image image)
        {
            using (var fd = Dlib.GetFrontalFaceDetector())
            {
                var img = ((Bitmap)image).ToArray2D<RgbPixel>();

                // find all faces in the image
                var faces = fd.Operator(img);
                foreach (var face in faces)
                {
                    // draw a rectangle for each face
                    Dlib.DrawRectangle(img, face, color: new RgbPixel(0, 255, 255), thickness: 4);
                }

                var finalImage = img.ToBitmap();
                OnImageReady(finalImage);
            }
        }

        private void ImageProviderOnImageReady(object sender, GenericEventArgs<Image> e)
        {
            ProcessFrame(e.Item);
        }
    }
}